FROM tomcat:9.0.0.M21

MAINTAINER lucastheisen@pastdev.com

WORKDIR $CATALINA_HOME

RUN ["rm", "-rf", "webapps"]
RUN ["mkdir", "webapps"]

ADD target/docker-foo-service.war webapps/ROOT.war

EXPOSE 8080

CMD ["bin/catalina.sh", "run"]
