package com.pastdev.example;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pastdev.example.resource.BarResource;

import io.swagger.jaxrs.config.BeanConfig;

/**
 * Inspired by: https://github.com/swagger-api/swagger-core/wiki/Swagger-Core-Jersey-2.X-Project-Setup-1.5#using-the-application-class
 */
public class FooApplication extends Application {
	private static final Logger LOGGER = LogManager.getLogger(FooApplication.class);

	public FooApplication() {
		LOGGER.info("Creating FooApplication");

		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion("1.0.0");
		beanConfig.setSchemes(new String[]{"http"});
		beanConfig.setHost("localhost:8002");
		beanConfig.setBasePath("/api");
		beanConfig.setResourcePackage("com.pastdev.example.resource");
		beanConfig.setScan(true);
	}
	
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();

		LOGGER.info("Registering resources");
        resources.add(BarResource.class);

		LOGGER.debug("Registering swagger resources");
        resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        
        return resources;
    }
}