package com.pastdev.example.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Bar {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private String description;
	private String name;
	private Type type;

	public Bar description(String description) {
		this.description = description;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public Bar name(String name) {
		this.name = name;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public String toString() {
		try {
			return OBJECT_MAPPER.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return name;
		}
	}

	public Bar type(Type type) {
		this.type = type;
		return this;
	}

	public static enum Type {
		Establishment, Being, Object
	}
}
