package com.pastdev.example.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pastdev.example.model.Bar;
import com.pastdev.example.model.Bar.Type;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Inspired by:
 * https://github.com/swagger-api/swagger-core/wiki/Annotations-1.5.X
 */
@Path("/bar")
@Api(value = "bar")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class BarResource {
	private static final Logger LOGGER = LogManager.getLogger(BarResource.class);
	
	private List<Bar> allBars = new ArrayList<>();
	
	public BarResource() {
		LOGGER.debug("Creating BarResource");
		allBars.add(new Bar().name("Cheers").type(Type.Establishment).description("A locals watering hole"));
		allBars.add(new Bar().name("Crowbar").type(Type.Object).description("A long piece of metal"));
		allBars.add(new Bar().name("Barney").type(Type.Being).description("A big purple dinosaur"));
	}

	@GET
	@Path("/list")
	@ApiOperation(value = "List all bars", notes = "All bars will be listed", response = Bar.class, responseContainer = "List")
	public Response listBars() {
		LOGGER.debug("listBars returns: {}", allBars);
		return Response.ok(allBars).build();
	}
}
